l = []

while True:
    inp = input("Enter number:")
    if inp in "exit":
        break

    l.append(int(inp))

print(l)


def fn(x):
    if x == 1:
        next
    elif x == -1:
        next
    else:
        return (x ** 3 + 2 * x) / (x ** 2 - 1)


new_list = [fn(x) for x in l]


print(new_list)